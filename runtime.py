import sys
import time
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib')
import dd
from driver import  spe
import picam as pi
import logging
import numpy as np
import IPython
from widget import Parameter
import matplotlib.pylab as plt
import datetime
import os
import shutil

logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)-13s %(name)-5s %(levelname)-3s %(message)s")


# Settings in SFH to transfer
parameterSet = 'picam'
params = {
        'rcm': pi.PicamParameter_ReadoutControlMode, 
            #FullFrame = 1
            #FrameTransfer = 2
            #Interline = 5
            #RollingShutter = 8
            #Kinetics = 3
            #SpectraKinetics = 4
            #Dif = 6
            #SeNsR = 7
        'vsr': pi.PicamParameter_VerticalShiftRate,
        'adcq': pi.PicamParameter_AdcQuality,
            #LowNoise = 1
            #HighCapacity = 2
            #HighSpeed = 4
            #ElectronMultiplied = 3
        'adcs': pi.PicamParameter_AdcSpeed, #MHz
        'adcemg': pi.PicamParameter_AdcEMGain,
        'adcag': pi.PicamParameter_AdcAnalogGain,
            #Low = 1
            #Medium = 2
            #High = 3
        'expt': pi.PicamParameter_ExposureTime, # in ms
        'roc': pi.PicamParameter_ReadoutCount,
        'clean_before_exposure': pi.PicamParameter_CleanBeforeExposure,
        'clean_until_trigger': pi.PicamParameter_CleanUntilTrigger,
        'clean_serial_register': pi.PicamParameter_CleanSerialRegister,
        'CleanCycleCount': pi.PicamParameter_CleanCycleCount,
        #'ts': pi.PicamParameter_TriggerSource,
            #None = 3
            #Internal = 2
            #External = 1
        'tr':pi.PicamParameter_TriggerResponse,#
            #1 no tirgger
            #2 readout per trigger
            #5 start on single trigger
        'stsp': pi.PicamParameter_SensorTemperatureSetPoint
        }
# Other parameters required
# roi, nf -> num frames

# Fake settings for testing without sfh libraries
parameter_set = {'rcm':2,
        'vsr':0.6,
        'adcq':3,
        'adcs':20,
        'adcemg':20,
        'adcag':1,
        'expt':0.,
        'roc':7200, 
        'tr':2,
        #'ts':2,
        'clean_before_exposure':0.,
        'clean_until_trigger':0.,
        'clean_serial_register':0.0,
        'CleanCycleCount':0.,
        'stsp':-70,
        #'roi':np.round(np.linspace(0,512,num=25,endpoint=False)),
        'roi':np.array([0,30, 62, 94, 126, 159, 190, 225, 255, 287, 319, 351, 384, 417]),#np.array([0,30, 62, 93, 124, 156, 189, 222, 253, 285, 318, 351, 384, 417]),
        'nf':1 } # not needed

def acquire(lib, cam, num_frames=3, nrois=25):
    readout_stride = cam.get(pi.PicamParameter_ReadoutStride)
    readout_count = cam.get(pi.PicamParameter_ReadoutCount)
    frames = np.empty(( readout_count, 512, nrois), "<u2")

    cam.start_acquisition()
    for i in range(readout_count+1):
        data,status = cam.wait_for_acquisition_update()
        logger.info("frame: %s"%(str(i)))
        if data.readout_count == 0:
            break
        data = cam.get_data(data, readout_stride).view("<u2").reshape((nrois,512)).T
        
        frames[i] = data
    
    cam.stop_acquisition()
    
    return frames

def acquire_non_reading(lib, cam, num_frames=3, nrois=25):
    readout_stride = cam.get(pi.PicamParameter_ReadoutStride)
    readout_count = cam.get(pi.PicamParameter_ReadoutCount)
    frames = np.empty(( readout_count, 512, nrois), "<u2")

    cam.start_acquisition()
    i = 0
    while cam.is_acquisition_running():
        data,status = cam.wait_for_acquisition_update()
        if data.readout_count == 0:
            break
        data_numpy = cam.get_data(data, readout_stride).view("<u2").reshape((data.readout_count,nrois,512))
        
        frames[i:(i+data.readout_count)] = np.swapaxes(data_numpy,1,2)
        logger.info("frame %s", str(i))
        i += data.readout_count
    
    logging.info("final number of frames collected: %d"%i) 
    cam.stop_acquisition()
    
    return frames

def getParameters():
    parameters = {}
    for i in self.cameraThread.cam.get_parameters():
        p = self.cameraThread.get_parameter_info(i)
        parameters[p.name] = p
        
    return parameters

def set_header_data(header, cam, lib):
    parameters = {}
    for i in cam.get_parameters():
        parameters[lib.get_string(pi.PicamEnumeratedType_Parameter, i)] = cam.get(i) 
    
    ###Update SPE header with camera parameter values
    header.AvGain = parameters['EM Gain']
    header.AnalogGain = parameters['ADC Analog Gain']#?
    header.readoutMode = parameters['Readout Control Mode']  
    header.WindowSize = parameters['Kinetics Window Height'] 
    header.xDimDet =  parameters['Sensor Active Width']
    header.exp_sec =  parameters['Exposure Time']
    header.yDimDet = parameters['Sensor Active Height'] 
    header.date =  bytes(datetime.datetime.now().strftime("%d%b%Y"), 'utf-8')
    header.DetTemperature = parameters['Sensor Temperature Reading']
    header.cleans =  parameters['Clean Cycle Count']
    header.NumSkpPerCln =  parameters['Clean Section Final Height']
    header.ADCresolution =  parameters['ADC Bit Depth']#?
    #speFile.header.ADCrate =  parameters['ADC Speed']#?
    header.ReadoutTime = parameters['Readout Time Calculation']
    header.ContinuousCleansFlag = parameters['Clean Until Trigger']
    header.ExternalTriggerFlag = parameters['Trigger Response']#?
    
    '''
    #header.ControllerVersion = parameters['']
    #header.LogicOutput = parameters['']
    #header.AmpHiCapLowNoise =  parameters['']
    #header.mode = parameters['']
    #header.VChipXdim = parameters['']
    #header.VChipYdim = parameters['']
    #header.VirtualChipFlag = parameters['']
    #header.noscan = parameters['']
    #header.DetType = parameters['']
    #header.stdiode = parameters['']
    #header.DelayTime =  parameters['']
    #header.ShutterControl =  parameters['']
    #header.AbsorbLive = parameters['']
    #header.AbsorbMode =  parameters['']
    #header.CanDoVirtualChipFlag = parameters['']
    #header.ThresholdMinLive = parameters['']
    ##header.ThresholdMinVal =  parameters['']
    #header.ThresholdMaxLive = parameters['']
    #header.ThresholdMaxVal =  parameters['']
    #header.SpecAutoSpectroMode = parameters['']
    #header.SpecCenterWlNm =  parameters['']
    #header.SpecGlueFlag = parameters['']
    #header.SpecGlueStartWlNm =  parameters['']
    #header.SpecGlueEndWlNm =  parameters['']
    #header.SpecGlueMinOvrlpNm =  parameters['']
    #header.SpecGlueFinalResNm =  parameters['']
    #header.PulserType = parameters['']
    #header.CustomChipFlag = parameters['']
    #header.XPrePixels = parameters['']
    #header.XPostPixels = parameters['']
    #header.YPrePixels = parameters['']
    #header.YPostPixels = parameters['']
    #header.asynen = parameters['']
    #header.datatype =  parameters['']
    #header.PulserMode = parameters['']
    #header.PulserOnChipAccums =  parameters['']
    #header.PulserRepeatExp =  parameters['']
    #header.PulseRepWidth =  parameters['']
    #header.PulseRepDelay =  parameters['']
    #header.PulseSeqStartWidth = parameters['']
    #header.PulseSeqEndWidth =  parameters['']
    #header.PulseSeqStartDelay =  parameters['']
    #header.PulseSeqEndDelay =  parameters['']
    #header.PulseSeqIncMode = parameters['']
    #header.PImaxUsed = parameters['']
    #header.PImaxMode = parameters['']
    #header.PImaxGain = parameters['']
    #header.BackGrndApplied = parameters['']
    #header.PImax2nsBrdUsed = parameters['']
    #header.minblk =  parameters['']
    #header.numminblk =  parameters['']
    #header.SpecMirrorLocation =  parameters['']
    #header.SpecSlitLocation =  parameters['']
    #header.CustomTimingFlag = parameters['']
    #header.ExperimentTimeLocal =  parameters['']
    #header.ExperimentTimeUTC =  parameters['']
    #header.ExposUnits = parameters['']
    #header.ADCoffset =  parameters['']
    #header.ADCtype =  parameters['']
    #header.ADCbitAdjust =  parameters['']
    #header.gain =  parameters['']
    #header.Comments =  parameters['']
    #header.geometric =  parameters[''] # x01 - rotate, x02 - reverse, x04 flip
    #header.xlabel =  parameters['']
    #header.SpecMirrorPos =  parameters['']
    #header.SpecSlitPos =  parameters['']
    #header.AutoCleansActive = parameters['']
    #header.UseContCleansInst = parameters['']
    #header.AbsorbStripNum = parameters['']
    #header.SpecSlipPosUnits = parameters['']
    #header.SpecGrooves =  parameters['']
    #header.srccmp = parameters['']
    #header.scramble = parameters['']
    #header.lnoscan =  parameters[''] # Longs are 4 bytes
    #header.lavgexp =  parameters[''] # 4 bytes
    #header.TriggeredModeFlag = parameters[''] #?
    #header.sw_version =  parameters['']
    #header.type = parameters['']
    #header.flatFieldApplied = parameters['']
    #header.kin_trig_mode = parameters['']
    #header.dlabel =  parameters['']
    #header.PulseFileName =  parameters['']
    #header.AbsorbFileName =  parameters['']
    #header.NumExpRepeats =  parameters['']
    #header.NumExpAccums =  parameters['']
    #header.YT_Flag = parameters['']
    #header.clkspd_us =  parameters['']
    #header.HWaccumFlag = parameters['']
    #header.StoreSync = parameters['']
    #header.BlemishApplied = parameters['']
    #header.CosmicApplied = parameters['']
    #header.CosmicType = parameters['']
    #header.CosmicThreshold =  parameters['']
    #header.MaxIntensity =  parameters['']
    #header.MinIntensity =  parameters['']
    #header.ylabel =  parameters['']
    #header.ShutterType =  parameters['']
    #header.shutterComp = parameters[''] 
    #header.clkspd =  parameters['']
    #header.interface_type =  parameters['']
    #header.NumROIsInExperiment = parameters['']
    #header.controllerNum =  parameters['']
    #header.SWmade =  parameters['']
    #header.NumROI = parameters['']
    #header.ROIinfblk =  parameters['']
    #header.FlatField =  parameters['']
    #header.background =  parameters['']
    #header.blemish =  parameters['']
    #header.file_header_ver =  parameters['']
    #header.YT_Info =  parameters['']
    #header.WinView_id =  parameters['']
    #header.xcalibration =  parameters['']
    #header.ycalibration =  parameters['']
    #header.Istring =  parameters['']
    #header.SpecType =  parameters['']
    #header.SpecModel =  parameters['']
    #header.PulseBurstUsed =  parameters['']
    #header.PulseBurstCount =  parameters['']
    #header.PulseBurstPeriod=  parameters['']
    #header.PulseBracketUsed =  parameters['']
    #header.PulseBracketType =  parameters['']
    #header.PulseTimeConstFast =  parameters['']
    #header.PulseAmplitudeFast =  parameters['']
    #header.PulseTimeConstSlow =  parameters['']
    #header.PulseAmplitudeSlow =  parameters['']
    #header.AvGainUsed = parameters['']
    #header.lastvalue =  parameters['']
    '''

    '''
    ##Below are the picam parameter names that have not been matched to the winspec header attributes

    Frame Size
    Frame Stride
    Frames per Readout
    Readout Stride
    Pixel Bit Depth
    Sensor Secondary Masked Height
    Sensor Active Left Margin
    Sensor Active Top Margin
    Sensor Active Right Margin
    Sensor Active Bottom Margin
    Sensor Masked Height
    Sensor Masked Top Margin
    Sensor Masked Bottom Margin
    Sensor Secondary Active Height
    Sensor Active Extended Height
    Pixel Width
    Pixel Height
    Gap Width
    Gap Height
    Readout Rate Calculation
    Frame Rate Calculation
    Online Readout Rate Calculation
    Sensor Temperature Status
    Orientation
    Readout Orientation
    CCD Characteristics
    Exact Readout Count Maximum
    Active Width
    Active Height
    Active Left Margin
    Active Top Margin
    Active Right Margin
    Active Bottom Margin
    Masked Height
    Masked Top Margin
    Clean Section Final Height Count
    Clean Cycle Height
    Masked Bottom Margin
    Sensor Temperature Set Point
    Shutter Closing Delay
    Shutter Opening Delay
    Readout Count
    Time Stamp Bit Depth
    Frame Tracking Bit Depth
    Vertical Shift Rate
    Shutter Delay Resolution
    Clean Serial Register
    Disable Cooling Fan
    Normalize Orientation
    Invert Output Signal
    Disable Data Formatting
    Track Frames
    Clean Before Exposure
    Correct Pixel Bias
    Shutter Timing Mode
    Trigger Determination
    Output Signal
    ADC Quality
    Pixel Format
    Time Stamps
    Time Stamp Resolution
    ROIs
    '''

readSFH = False
readConfig = True
privateAquisition = False
save_SPE = True

# Open Camera
with pi.Library() as lib:
    cam = pi.Camera()
    try:
        cam.open_first()
    except pi.Error:
        cam.open(cam.connect_demo(
            pi.PicamModel_ProEMHS512BExcelon, "12345678"))
    cid = cam.get_id()
    model = lib.get_string(pi.PicamEnumeratedType_Model, cid.model)
    logger.info("model: %s, serial: %s, sensor: %s", model,
                cid.serial_number.decode(), cid.sensor_name.decode())

    logger.info("firmware details %s", cam.get_firmware_details())

    # Read temperature to set from sfh and apply to camera to cool down
    if readSFH:
        sfhfile = "/afs/ipp/u/cxrs/CMR_diag/CMR00000.sfh"
        # Open sfh
        sfh = sfh.SFH()
        sfh.Open(sfhfile)
        parameter_set = sfh.Readparset("picam")
        # Close to let modifications
        sfh.Close()

    if readConfig:    
        # to be replace by readSFH
        logger.info('\033[91m Reading parameter config file!\033[0m')
        with open('PICAM_config.txt', 'r') as reader:
            for line in reader:
                configParam = str(line).split(':')[0]
                configParamValue = int(str(line).split(':')[1])
                parameter_set[configParam] = configParamValue 
                
        logger.info('\033[91m Finished reading parameter config file!\033[0m')   
        
    # Set camera temperature and turn on fan
    logger.info("firmware details %s", cam.get_firmware_details())
    logger.info("Turn on cooling and set temp to %s",parameter_set['stsp'])
    cam.set(pi.PicamParameter_DisableCoolingFan, False)
    cam.set(pi.PicamParameter_SensorTemperatureSetPoint,parameter_set['stsp'])
    cam.commit()
    
    
    while dd.wait():
        ## Open again sfh and get parameters
        if readSFH:
            sfh.Open(sfhfile)
            parameter_set = sfh.Readparset("picam")
            
        if readConfig: 
            # to be replace by readSFH
            logger.info('\033[91m Reading parameter config file!\033[0m')
            with open('PICAM_config.txt', 'r') as reader:
                for line in reader:
                    configParam = str(line).split(':')[0]
                    configParamValue = int(str(line).split(':')[1])
                    parameter_set[configParam] = configParamValue 
                    
            logger.info('\033[91m Finished reading parameter config file!\033[0m') 
            
        # Check temperature
        if cam.get(pi.PicamParameter_SensorTemperatureReading) != parameter_set['stsp']:
            logger.warning("Camera did not reach the set temperature: %.1f instead of %.1f"\
                    %(cam.get(pi.PicamParameter_SensorTemperatureReading),parameter_set['stsp']))
        # Open shutter
        logger.info("Open shutter")
        cam.set(pi.PicamParameter_ShutterTimingMode,pi.PicamShutterTimingMode_AlwaysOpen)
        # Set up camera
        for k,v in params.items():
            logger.info("%s"%k)
            try:
                cam.set(v,parameter_set[k])
            except:
                logger.error("%s, %s, %s Failed"%(k,v,parameter_set[k]))
        # Create and set ROIs
        rois = []
        jroi = 0
        for start_y in parameter_set['roi']:
            # No bining in the x direction ans spectral mode in the y direction
            if jroi == parameter_set['roi'].size-1:
                rois.append(((0,512,1),\
                        (int(start_y),int(512-start_y)-1,int(512-start_y) -1)))
            else:
                rois.append(((0,512,1),\
                        (int(start_y),int(parameter_set['roi'][jroi+1]-start_y),\
                        int(parameter_set['roi'][jroi+1]-start_y) )))#AJVV #-1)))
            jroi += 1
        logger.info("ROIs")
        cam.set(pi.PicamParameter_Rois, rois)
        try:
            cam.commit()
        except pi.Error as err:
            logger.info("failed commit: %s", [
                lib.get_string(pi.PicamEnumeratedType_Parameter, i)
                for i in err.fails])

        logger.info("ReadoutTimeCalculation: %f"%(cam.get(pi.PicamParameter_ReadoutTimeCalculation)))
        # Acquire 
        #IPython.embed()
        logger.info("Acquiring")
        frames = acquire_non_reading(lib, cam, num_frames=parameter_set['nf'], nrois=parameter_set['roi'].size)
        logger.info("Finished acquiring")
        
        if save_SPE:
            ###Load dummy SPE file
            speFile = spe.SpeFile('dummy_.SPE')
            
            ###Update dummy header
            set_header_data(speFile.header, cam, lib)
            
            tmp_dir = '/afs/ipp-garching.mpg.de/home/e/e114/tmp/CGF/'
            if not privateAquisition:
                shot = str(dd.getLastAUGShotNumber())
            else:
                try:
                    shotNumberFile = open(tmp_dir+'shotnum.txt', 'r')
                    shot = shotNumberFile.readline()
                    shotNumberFile.close()
                    
                except:
                    shot = '_dummy_'
                    
            spe_tmp_file_name = 'CGF'+shot+'_.spe'
            spe_file_name = 'CGF'+shot+'.SPE'

            logger.info('\033[91m Writing %s file...\033[0m',spe_tmp_file_name)
            speFile._write(spe_tmp_file_name,frames)
            logger.info('\033[92m SPE file written!\033[0m')
            
            logger.info('\033[91m Moving SPE file to CGF temp!\033[0m')
            shutil.move(spe_tmp_file_name, tmp_dir + spe_tmp_file_name)
            os.rename(tmp_dir + spe_tmp_file_name, tmp_dir + spe_file_name)
            logger.info('\033[92m SPE file moved to CGF temp!\033[0m')
        
        logger.info("done")

