#!/bin/sh
# Default variables
START_DIR=`dirname $0`
START_DIR=`cd $START_DIR; pwd`
echo "Start_DIR is" $START_DIR

USR_LIB=/usr/lib64
USR_LOC_LIB=/usr/local/lib64

INSTALL_ROOT=$START_DIR
PI_ROOT=`cd ..; pwd`
PLEORA_ROOT=`cd $START_DIR/../../pleora; pwd`

REMOVE_LINKS=yes
INSTALL_DIR_OVERWRITE=yes

# Initial screen
clear
echo -n "Remove Picam SDK installed in directory '$INSTALL_ROOT' (yes/no) ? "
read ANSWER
if [ ! "$ANSWER" = "yes" ]; then
  echo "Uninstall cancelled."
  exit;
fi

if [ ! -w $INSTALL_ROOT ]; then
  echo "You do not have write permissions to '$INSTALL_ROOT'."
  echo "Run installer as super user (root)."
  exit 1
fi

echo "Removing software on $INSTALL_ROOT"
if [ -d $PI_ROOT ]; then
    rm -rf $PI_ROOT 
fi

#pleora removal
if [ -d $PLEORA_ROOT ]; then
    rm -rf $PLEORA_ROOT 
fi

if [ "$REMOVE_LINKS" = "yes" ]; then
  rm -rf $USR_LOC_LIB/libPv*
  rm -rf $USR_LOC_LIB/libEb*
  rm -rf $USR_LOC_LIB/libPt*
  rm -rf $USR_LOC_LIB/libEbuser*
  rm -rf $USR_LOC_LIB/liblog*
  rm -rf $USR_LOC_LIB/libapr*
  rm -rf $USR_LOC_LIB/libG*
  rm -rf $USR_LOC_LIB/liblog*
  rm -rf $USR_LOC_LIB/libM*
# Picam references
  rm -rf /var/run/pits
  rm -rf $USR_LOC_LIB/libpi*
  rm -rf $USR_LOC_LIB/*.dat
  rm -rf /etc/ld.so.conf.d/picam.conf
fi


echo "Removal complete."
echo ""
echo ""

