#ifndef __LFT_VERSION_H__
#define __LFT_VERSION_H__

// *****************************************************************************"
//
//     Copyright (c) 2011, Pleora Technologies Inc., All rights reserved."
//
// *****************************************************************************"

// Automatically generated resources file"

#define LFT_MODULE_LONG_NAME "eBUS Universal Pro For Ethernet"
#define LFT_MODULE_NAME      "ebUniversalProForEthernet"
#define LFT_COMPANY          "Pleora Technologies Inc."
#define LFT_COPYRIGHT        "Copyright (c) 2002-2016"
#define LFT_VERSION          "5.1.7 build 3988"
#define LFT_VERSION_MAJOR    5
#define LFT_VERSION_MINOR    1
#define LFT_VERSION_SUB      7
#define LFT_VERSION_BUILD    3988

#endif /* __LFT_VERSION_H__ */

