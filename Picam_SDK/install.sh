#!/bin/bash

# Default variables
PICAM_VERSION=5.10.1
LIBPICAM_VER=5.10.1
LIBPIDI_VER=11.2.0
LIBPIDA_VER=7.1.0
LIBPICC_VER=9.2.0
LIBPIAC_VER=3.0.1
# 3rd Party libraries
LIBFTDI_VER=1.4.6

#USR_LIB=/usr/lib64
USR_LIB=/usr/lib
#LIB=/lib64
#USR_LOC_LIB=/usr/local/lib64
USR_LOC_LIB=/usr/local/lib

START_DIR=`dirname $0`
PLEORA_DIR=`cd $START_DIR/pleora; pwd`


INSTALL_PLEORA_ROOT=/opt/pleora/ebus_sdk

INSTALL_LINKS=yes
INSTALL_DIR_OVERWRITE=yes

# Initial screen
clear
echo "Princeton Instruments Picam SDK v$PICAM_VERSION for Linux"
echo ""
echo ""
echo "Installing GigE support..."
echo ""
echo ""

cd $PLEORA_DIR
. install.sh

INSTALL_LINKS=yes


#echo "Installing software on $INSTALL_PLEORA_ROOT"

# ---------------------------------------------------------------------------------
cd ..
START_PI_DIR=`dirname $0`
START_PI_DIR=`cd $START_PI_DIR/pi; pwd`

INSTALL_ROOT='/opt/PrincetonInstruments/picam'

INSTALL_PARENT=`dirname "$INSTALL_ROOT"`
if [ ! -d $INSTALL_PARENT ]; then
  mkdir $INSTALL_PARENT
fi
INSTALL_PARENT=`cd "$INSTALL_PARENT"; pwd`
if [ ! -d "$INSTALL_PARENT" ]; then
  echo "The directory "$INSTALL_PARENT" does not exist"
  exit 1
fi

if [ ! -w "$INSTALL_PARENT" ]; then
  echo "You do not have write permissions to '$INSTALL_PARENT'."
  echo "Run installer as super user (root)."
  exit 1
fi

if [ -d "$INSTALL_ROOT" ]; then
  echo -n "The destination directory already exists. Overwrite [$INSTALL_DIR_OVERWRITE]? "
  read ANSWER
  until [ "$ANSWER" = "yes" -o "$ANSWER" = "no" -o "$ANSWER" = "" ]; do
    echo "Please enter yes or no"
    echo -n "The destination directory already exists. Overwrite [$INSTALL_DIR_OVERWRITE]? "
    read ANSWER
  done
  if [ ! "$ANSWER" = "" ]; then
    INSTALL_DIR_OVERWRITE=$ANSWER
  fi
fi

if [ ! $INSTALL_DIR_OVERWRITE = "yes" ]; then
  echo "Installation aborted"
  exit 1;
fi

if [ "$INSTALL_LINKS" = "yes" ]; then
  if [ ! -w $USR_LOC_LIB ]; then
    echo "You do not have write permissions to '$USR_LOC_LIB'."
    exit 1;
  fi
fi


echo "Installing software on $INSTALL_ROOT"
mkdir -p $INSTALL_ROOT

[ -d "$INSTALL_ROOT/documentation" ] || mkdir "$INSTALL_ROOT/documentation"
[ -d "$INSTALL_ROOT/includes" ]      || mkdir "$INSTALL_ROOT/includes"
[ -d "$INSTALL_ROOT/runtime" ] || mkdir "$INSTALL_ROOT/runtime"
[ -d "$INSTALL_ROOT/samples" ]       || mkdir "$INSTALL_ROOT/samples"
[ -d "$INSTALL_ROOT/samples/projects" ]   || mkdir "$INSTALL_ROOT/samples/projects"
[ -d "$INSTALL_ROOT/samples/projects/gcc" ]   || mkdir "$INSTALL_ROOT/samples/projects/gcc"
[ -d "$INSTALL_ROOT/samples/source\ code" ]   || mkdir "$INSTALL_ROOT/samples/source code"
[ -d "$INSTALL_ROOT/samples/source\ code/platform\ independent" ]   || mkdir "$INSTALL_ROOT/samples/source code/platform independent"
[ -d "$INSTALL_ROOT/samples/source\ code/linux" ] || mkdir "$INSTALL_ROOT/samples/source code/linux"

cp -f $START_PI_DIR/uninstall.sh $INSTALL_ROOT
cp -f $START_PI_DIR/picam.conf /etc/ld.so.conf.d
cp -f $START_PI_DIR/misc/picam.sh /etc/profile.d/
cp -f $START_PI_DIR/misc/picam-tmp.conf /usr/lib/tmpfiles.d/

if [ ! "`ls $START_PI_DIR/documentation/* 2>/dev/null`" = "" ]; then
  cp -rf $START_PI_DIR/documentation/* $INSTALL_ROOT/documentation
fi

if [ ! "`ls $START_PI_DIR/runtime/* 2>/dev/null`" = "" ]; then
  cp -f $START_PI_DIR/runtime/*.dat $INSTALL_ROOT/runtime
  cp -f $START_PI_DIR/runtime/libpicam.so.$LIBPICAM_VER $INSTALL_ROOT/runtime/libpicam.so.$LIBPICAM_VER
  cp -f $START_PI_DIR/runtime/libpidi.so.$LIBPIDI_VER $INSTALL_ROOT/runtime/libpidi.so.$LIBPIDI_VER
  cp -f $START_PI_DIR/runtime/libpicc.so.$LIBPICC_VER $INSTALL_ROOT/runtime/libpicc.so.$LIBPICC_VER
  cp -f $START_PI_DIR/runtime/libpida.so.$LIBPIDA_VER $INSTALL_ROOT/runtime/libpida.so.$LIBPIDA_VER
  cp -f $START_PI_DIR/runtime/libpiac.so.$LIBPIAC_VER $INSTALL_ROOT/runtime/libpiac.so.$LIBPIAC_VER

  cp -f $START_PI_DIR/misc/libftd2xx.so.$LIBFTDI_VER $USR_LOC_LIB/libftd2xx.so.$LIBFTDI_VER
fi

if [ ! "`ls $START_PI_DIR/includes/* 2>/dev/null`" = "" ]; then
  cp -rf $START_PI_DIR/includes/* $INSTALL_ROOT/includes
  chmod -R a+r $INSTALL_ROOT/includes
fi

if [ ! "`ls $START_PI_DIR/samples/* 2>/dev/null`" = "" ]; then
  cp -rf $START_PI_DIR/samples/* $INSTALL_ROOT/samples
  chmod -R ug+w "$INSTALL_ROOT/samples/source code/platform independent"
  chmod -R ug+w "$INSTALL_ROOT/samples/source code/linux"
fi

if [ "$INSTALL_LINKS" = "yes" ]; then
  echo "Installing library symbolic links in $USR_LOC_LIB"
  cp -rs $INSTALL_ROOT/runtime/libpicam.so.$LIBPICAM_VER $USR_LOC_LIB/libpicam.so
  cp -rs $INSTALL_ROOT/runtime/libpicam.so.$LIBPICAM_VER $USR_LOC_LIB/libpicam.so.0
  cp -rs $INSTALL_ROOT/runtime/libpidi.so.$LIBPIDI_VER $USR_LOC_LIB/libpidi.so
  cp -rs $INSTALL_ROOT/runtime/libpidi.so.$LIBPIDI_VER $USR_LOC_LIB/libpidi.so.0
  cp -rs $INSTALL_ROOT/runtime/libpicc.so.$LIBPICC_VER $USR_LOC_LIB/libpicc.so
  cp -rs $INSTALL_ROOT/runtime/libpicc.so.$LIBPICC_VER $USR_LOC_LIB/libpicc.so.0
  cp -rs $INSTALL_ROOT/runtime/libpida.so.$LIBPIDA_VER $USR_LOC_LIB/libpida.so
  cp -rs $INSTALL_ROOT/runtime/libpida.so.$LIBPIDA_VER $USR_LOC_LIB/libpida.so.0
  cp -rs $INSTALL_ROOT/runtime/libpiac.so.$LIBPIAC_VER $USR_LOC_LIB/libpiac.so
  cp -rs $INSTALL_ROOT/runtime/libpiac.so.$LIBPIAC_VER $USR_LOC_LIB/libpiac.so.0

  cp -rs $USR_LOC_LIB/libftd2xx.so.$LIBFTDI_VER $USR_LOC_LIB/libftd2xx.so.0
# works for all dat files 
  ln -s $INSTALL_ROOT/runtime/*.dat $USR_LOC_LIB
  
  ldconfig $USR_LOC_LIB
fi

chmod 777 $INSTALL_ROOT/uninstall.sh
echo ""
echo "Picam v$PICAM_VERSION Installation complete."
echo ""
echo "NOTE:"
echo "Please configure the firewall to allow all traffic through"
echo "the ethernet adapter connected to the camera."
echo ""
echo "Firewall->Trusted Interfaces->eth<X>"
echo ""
echo ""
