PICam Python wrapper and SDK
=============================

* Before use
    - source setup_env.sh
    - mkdir $HOME/.local/tmp
* Works only under lxpicamtst (to make working under other machine re-compile interceptor
* Works from python3, for the widget it needs pyqtgraph.
* If you get an error like ``Failed to create process lock ...(named: PrincetonInstruments:: ... ::CrossLock)`` and you are sure you are the only one running the camera, please clear the ``/dev/shm``


TODO
----

* Check consistency parameters (cleanings and so on.)
* Debug Widget
* Save ROI in Widget
* Save data from Widget


HOW TO SETUP THE LINUX PI
-------------------------
Configuration of the eth card (e.g. enp5s0):

* auto enp5s0
* iface enp5s0 inet static
* address 169.254.0.1
* netmask 255.255.0.0

ifconfig to check that network adapter is setup correct. Test if the camera is connected correctly via 'ping -b 169.254.255.255'

For more details see pictures
