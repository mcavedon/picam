#!/usr/bin/env python
import os
import ctypes
import numpy as np
import picam as pi
import widget
from pyqtgraph.Qt import QtCore, QtGui
import logging
import sys
from argparse import ArgumentParser

parser = ArgumentParser(description='PICam Widget')
parser.add_argument("-d", "--demo",
                    action="store_true", dest="demo", default=False,
                    help="don't print status messages to stdout")
args = parser.parse_args()

app = QtGui.QApplication(sys.argv)
logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)-13s %(name)-5s %(levelname)-3s %(message)s")

logger = logging.getLogger(__name__)
logger.info("Open libraries")
with pi.Library() as lib:
    logger.info("Open camera")
    cam = pi.Camera()
    if args.demo:
        cam.open(cam.connect_demo(
            pi.PicamModel_ProEMHS512BExcelon, "12345678"))
    else:
        try:
            cam.open_first()
        except pi.Error:
            cam.open(cam.connect_demo(
                pi.PicamModel_ProEMHS512BExcelon, "12345678"))
    cid = cam.get_id()
    model = lib.get_string(pi.PicamEnumeratedType_Model, cid.model)
    print("model: %s, serial: %s, sensor: %s", model,
                cid.serial_number.decode(), cid.sensor_name.decode())

    print("firmware details %s", cam.get_firmware_details())
    print(cam.get(pi.PicamParameter_SensorTemperatureReading))

    logger.info("Launch widget")
    cam = widget.picamWidget(app,cam,lib)
    cam.show()
    sys.exit(app.exec_())

