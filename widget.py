from pyqtgraph.Qt import QtCore, QtGui
import ctypes
import datetime
import pyqtgraph as pg
import sys
from pyqtgraph.dockarea import *
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject, Qt
import picam as pi
import logging
import numpy as np
import time
from PyQt5.QtWidgets import QTableWidget, QTableWidgetItem, QFileDialog, QSlider
from PyQt5 import QtWidgets
import collections
from pathlib import Path
import xml.etree.ElementTree as ET
from IPython import embed
import time

from driver import  spe

ydim = 300

logger = logging.getLogger(__name__)

class Parameters(collections.OrderedDict):
    def __init__(self):
        super(Parameters, self).__init__()

    def save(self,filename=None):
        logging.info("Saving configuration file %s"%filename)
        data = ET.Element('parameters')
        for k,p in self.items():
            if p.access == 'Read/Write':
                if p.name == 'ROIs':
                    continue
                items = ET.SubElement(data, "parameter")
                i = ET.SubElement(items, 'name')
                i.text = p.name
                i = ET.SubElement(items, 'index')
                i.text = "%d"%p.i
                i = ET.SubElement(items, 'value')
                i.text = "%f"%p.value
        self.indent(data)
        tree = ET.ElementTree(data)
        tree.write(filename)
        logging.info("Done!")

    def load(self,filename=None):
        logging.info("Loading configuration file %s"%filename)
        tree = ET.parse(filename)
        root = tree.getroot()
        for element in root:
            if element.tag == 'parameter':
                if type(self[element[0].text].value) == type(1):
                    self[element[0].text].value = int(float(element[2].text))
                else:
                    self[element[0].text].value = float(element[2].text)
                self[element[0].text].set(self[element[0].text].value)
                self[element[0].text].update()
        logging.info("Done!")

    def indent(self, elem, level=0):
        i = "\n" + level*"  "
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + "  "
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i

class QHSeperationLine(QtWidgets.QFrame):
  '''
  a horizontal seperation line\n
  '''
  def __init__(self):
    QtWidgets.QFrame.__init__(self)
    self.setMinimumWidth(1)
    self.setFixedHeight(20)
    self.setFrameShape(QtWidgets.QFrame.HLine)
    self.setFrameShadow(QtWidgets.QFrame.Sunken)
    self.setSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Minimum)
    return

class Parameter:
    def __init__(self,i,cam,lib):
        self.cam = cam
        self.lib = lib
        self.i = i
        self.name = self.lib.get_string(pi.PicamEnumeratedType_Parameter, i)
        self.access = self.lib.get_string(pi.PicamEnumeratedType_ValueAccess, self.cam.get_parameter_value_access(i))
        self.value_typ = self.lib.get_string(pi.PicamEnumeratedType_ValueType, self.cam.get_parameter_value_type(i))
        self.value = self.cam.get(i)
        if self.cam.get_parameter_value_type(i) == pi.PicamValueType_Enumeration:
            self.value_str = self.lib.get_string(self.cam.get_parameter_enumerated_type(i), self.value)
        self.constraint_typ = self.lib.get_string(pi.PicamEnumeratedType_ConstraintType, self.cam.get_parameter_constraint_type(i))
        if self.cam.get_parameter_constraint_type(i) == pi.PicamConstraintType_Range:
            self.range = np.array(self.cam.get_parameter_range_constraint(i))
        elif self.cam.get_parameter_constraint_type(i) == pi.PicamConstraintType_Collection:
            self.collection = self.cam.get_parameter_collection_constraint(i)
            if (self.cam.get_parameter_value_type(i) ==
                    pi.PicamValueType_Enumeration):
                self.collection_str = [self.lib.get_string(
                    self.cam.get_parameter_enumerated_type(i), int(j))
                    for j in self.collection]
        self.create_widget()

    def create_widget(self):
        if self.access == "Read/Write":
            if self.constraint_typ == 'Range':
                lbl = QtGui.QLabel()
                if self.name == "Exposure Time":
                    lbl.setText(self.name+' [ms]')
                else:
                    lbl.setText(self.name)
                self.line = QtGui.QLineEdit()
                self.line.setText(str(self.value))
                self.line.setFixedWidth(70)
                self.line.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
                self.layout = QtGui.QHBoxLayout()
                self.layout.addWidget(lbl)
                self.layout.addWidget(self.line)
            elif self.constraint_typ == 'Collection':
                lbl = QtGui.QLabel()
                lbl.setText(self.name)
                self.combo = QtGui.QComboBox()
                if hasattr(self,'collection_str'):
                    current_item = 0
                    for jitem,item in enumerate(self.collection_str):
                        self.combo.addItem(item)
                        if item == self.value_str:
                            current_item = jitem
                    self.combo.setCurrentIndex(current_item)
                else:
                    current_item = 0
                    for jitem,item in enumerate(self.collection):
                        self.combo.addItem("%.2f"%item)
                        if item == self.value:
                            current_item = jitem
                    self.combo.setCurrentIndex(current_item)
                self.layout = QtGui.QHBoxLayout()
                self.layout.addWidget(lbl)
                self.layout.addWidget(self.combo)

    def update(self):
        if self.cam.get_parameter_value_type(self.i) == pi.PicamValueType_Enumeration:
            self.value_str = self.lib.get_string(self.cam.get_parameter_enumerated_type(self.i), self.value)
        if self.constraint_typ == 'Range':
            self.line.setText(str(self.value))
        if self.constraint_typ == 'Collection':
            if hasattr(self,'collection_str'):
                current_item = 0
                for jitem,item in enumerate(self.collection_str):
                    if item == self.value_str:
                        current_item = jitem
                self.combo.setCurrentIndex(current_item)
            else:
                current_item = 0
                for jitem,item in enumerate(self.collection):
                    if item == self.value:
                        current_item = jitem
                self.combo.setCurrentIndex(current_item)

    def set(self,value):
        self.value = value
        if self.access == "Read/Write":
            self.cam.set(self.i,self.value)
        self.value = self.cam.get(self.i)
        if self.cam.get_parameter_value_type(self.i) == pi.PicamValueType_Enumeration:
            self.value_str = self.lib.get_string(self.cam.get_parameter_enumerated_type(self.i), self.value)

    def set_from_widget(self):
        if self.access == "Read/Write":
            if self.constraint_typ == 'Range':
                self.cam.set(self.i,np.float(self.line.text()))
            if self.constraint_typ == 'Collection':
                if hasattr(self,'collection_str'):
                    self.cam.set(self.i,\
                            self.collection[self.combo.currentIndex()])
                else:
                    self.cam.set(self.i,self.collection[self.combo.currentIndex()])
        self.value = self.cam.get(self.i)
        if self.cam.get_parameter_value_type(self.i) == pi.PicamValueType_Enumeration:
            self.value_str = self.lib.get_string(self.cam.get_parameter_enumerated_type(self.i), self.value)

class CameraThread(QtCore.QThread):
    new_data = pyqtSignal()

    def __init__(self,lib,cam):
        QtCore.QThread.__init__(self)
        self.cam = cam
        self.lib = lib
        self.startAcq = False
        self.startFcs = False
        self.stopAcq = True
        self.stopFcs = True
        self.t_value = 0.
        self.xdim = np.int(self.cam.get(pi.PicamParameter_ActiveWidth))
        self.ydim = np.int(self.cam.get(pi.PicamParameter_ActiveHeight))
        self.full_chip_roi = [((0,self.xdim,1), (0,self.ydim,1))]
        self.data = []

    def run(self):
        while 1:
            if self.cam.is_acquisition_running():
                readout_stride = self.cam.get(pi.PicamParameter_ReadoutStride)
                readout_count = self.cam.get(pi.PicamParameter_ReadoutCount)
                # Focus
                if readout_count == 0:
                    print("readout_count is zero")
                    while self.cam.is_acquisition_running():
                        print("Acquisition is running")
                        data,status = self.cam.wait_for_acquisition_update()
                        print("Acquisition updated")
                        if data.readout_count == 0:
                            break
                        self.data = self.cam.get_data(data,\
                                readout_stride).view("<u2").reshape((self.ydim,self.xdim)).T
                        self.new_data.emit()
                else:
                # Acquire
                    frames = np.empty((readout_count, self.xdim, self.ydim), "<u2")
                    for i in range(readout_count+1):
                        data,status = self.cam.wait_for_acquisition_update()
                        if data.readout_count == 0:
                            break
                        self.data = self.cam.get_data(data,\
                                readout_stride).view("<u2").reshape((self.ydim,self.xdim)).T
                        frames[i] = self.data
                        self.new_data.emit()
                    self.data = frames[:i]
                    self.cam.stop_acquisition()
                    self.new_data.emit()
            time.sleep(0.01)

    def get_parameter_info(self, i):
        return Parameter(i,self.cam,self.lib)

    def configure(self):
        self.cam.commit()
        self.roi_to_dim()

    def read_configuration(self):
        pass

    def stop(self):
        self.cam.stop_acquisition()

    def acquire(self):
        self.cam.start_acquisition()

    def focus(self):
        self.cam.set(pi.PicamParameter_ReadoutCount,0)
        self.cam.commit()
        readout_stride = self.cam.get(pi.PicamParameter_ReadoutStride)
        # Create a buffer for 100 Readouts
        numpy_buffer = np.empty(readout_stride*100,dtype=pi.pibyte)
        buffer = pi.PicamAcquisitionBuffer()
        buffer.memory = numpy_buffer.__array_interface__['data'][0]
        buffer.memory_size = readout_stride*100
        self.cam.set_acquisition_buffer(buffer)
        self.cam.start_acquisition()
        
    def temperature(self):
        self.t_value = self.cam.get(pi.PicamParameter_SensorTemperatureReading)
        self.new_temperature.emit()

    def roi_to_dim(self):
        rois = self.cam.get_rois()
        self.xdim = np.int(rois[0][0][1]/rois[0][0][2])
        self.ydim = 0
        for r in rois:
            self.ydim += r[1][1]/r[1][2]
        self.ydim = np.int(self.ydim)

class picamWidget(QtGui.QMainWindow):
    def __init__(self,app,cam,lib):
        super(self.__class__, self).__init__()
        QtCore.pyqtRemoveInputHook() # to enable Ipython embed
        self.app = app
        # Start Camera Thread
        logging.info("Starting Camera Thread")
        self.cameraThread = CameraThread(lib,cam)
        self.cameraThread.new_data.connect(self.show_data)
        self.cameraThread.start()
        # Get all parameters
        logging.info("Read Parameter")
        self.parameters = Parameters()
        for i in self.cameraThread.cam.get_parameters():
            p = self.cameraThread.get_parameter_info(i)
            self.parameters[p.name] = p
        logging.info("Create Widget")
        self.resize(800,600)
        self.area = DockArea()
        self.setCentralWidget(self.area)
        self._dockData()
        self._dockExpSetup()
        self._dockROI()
        self._dockMotor()
        self._dockParams()
        self.area.addDock(self.dockImg, 'left')
        self.area.addDock(self.dockParams,'right')
        self.area.addDock(self.dockMotor,'above',self.dockParams)
        self.area.addDock(self.dockROI,'above',self.dockMotor)
        self.area.addDock(self.dockExpSetup,'above',self.dockROI)

    def _update_params(self):
        jrow = 0
        self.parameters.update()
        for k,v in self.parameters.items():
            if k == 'ROIs':
                continue
            if hasattr(v,"value_str"):
                self.tableWidget.setItem(jrow,1, QTableWidgetItem("%s"%v.value_str))
            else:
                self.tableWidget.setItem(jrow,1, QTableWidgetItem("%s"%v.value))
            jrow += 1

    def show_data(self):
        self.jtime = 0
        if len(self.cameraThread.data.shape) == 2:
            self.img.setImage(self.cameraThread.data,autoRange=True,autoLevels=True,autoHistogramRange=False)
            self._update_img_plot()
        else:
            self._update_time_line()
            self.img.setImage(self.cameraThread.data[self.jtime],autoRange=True,autoLevels=True,autoHistogramRange=False)
            logging.info("Acquisition done!")
        self.hist.setLevels(self.cameraThread.data.min(), self.cameraThread.data.max())

    def _update_data(self):
        self.img.setImage(self.cameraThread.data[self.jtime],autoRange=True,autoLevels=True,autoHistogramRange=False)

    def _update_time_line(self):
        self.timeLine.setRange(1, self.cameraThread.data.shape[0])
        self.timeLine.setValue(self.jtime)

    def _acquire(self):
        logging.info("Starting Camera Acquisition")
        self._configure()
        self.jtime = 0
        self.x = 0
        self.y = 0
        self.cameraThread.acquire()
        logging.info("Acquiring")

    def _stop(self):
        logging.info("Acquisition stopped")
        self.cameraThread.stop()

    def _focus(self):
        logging.info("Starting Focussing")
        self._configure()
        self.jtime = 0
        self.x = 0
        self.y = 0
        self.cameraThread.focus()

    def _save(self):
        home_dir = str(Path.home())
        fname = QFileDialog.getSaveFileName(self, 'Save file', home_dir)
        self.parameters.save(fname[0])

    def _load(self):
        home_dir = str(Path.home())
        fname = QFileDialog.getOpenFileName(self, 'Open file', home_dir)
        self.parameters.load(fname[0])
        self._configure()

    def _update_roi(self):
        nrow = self.ROItable.rowCount()
        self.rois = []
        for jrow in range(nrow):
            roi = ((int(self.ROItable.item(jrow, 0).text()),int(self.ROItable.item(jrow, 1).text()),int(self.ROItable.item(jrow, 2).text())),\
                   (int(self.ROItable.item(jrow, 3).text()),int(self.ROItable.item(jrow, 4).text()),int(self.ROItable.item(jrow, 5).text())))
            self.rois.append(roi)

    def _configure(self):
        if self.ROIbox.isChecked():
            self._update_roi()
            self.cameraThread.cam.set(pi.PicamParameter_Rois, self.rois)
        else:
            self.cameraThread.cam.set(pi.PicamParameter_Rois, self.cameraThread.full_chip_roi)
        for k,p in self.parameters.items():
            p.set_from_widget()
        self.cameraThread.configure()
        self._update_params()

    def _saveSPE(self):
        self.cameraThread.stop()
        logging.info("Acquisition stopped")

        speFile = spe.SpeFile('dummy_.SPE')
        #embed()
        ###Update SPE header with camera parameter values
        speFile.header.AvGain = self.parameters['EM Gain'].value
        speFile.header.AnalogGain = self.parameters['ADC Analog Gain'].value#?
        speFile.header.readoutMode = self.parameters['Readout Control Mode'].value 
        speFile.header.WindowSize = self.parameters['Kinetics Window Height'].value
        speFile.header.xDimDet =  self.parameters['Sensor Active Width'].value
        speFile.header.exp_sec =  self.parameters['Exposure Time'].value
        speFile.header.yDimDet = self.parameters['Sensor Active Height'].value 
        speFile.header.date =  bytes(datetime.datetime.now().strftime("%d%b%Y"), 'utf-8')
        speFile.header.DetTemperature = self.parameters['Sensor Temperature Reading'].value
        speFile.header.cleans =  self.parameters['Clean Cycle Count'].value
        speFile.header.NumSkpPerCln =  self.parameters['Clean Section Final Height'].value
        speFile.header.ADCresolution =  self.parameters['ADC Bit Depth'].value#?
        #speFile.header.ADCrate =  parameters['ADC Speed']#?
        speFile.header.ReadoutTime = self.parameters['Readout Time Calculation'].value
        speFile.header.ContinuousCleansFlag = self.parameters['Clean Until Trigger'].value
        speFile.header.ExternalTriggerFlag = self.parameters['Trigger Response'].value#?
        
        home_dir = str(Path.home())
        fname = QFileDialog.getSaveFileName(self, 'Save file', home_dir)

        frames = self.cameraThread.data
        
        logger.info('\033[91m Writing %s file...\033[0m',fname[0])
        speFile._write(fname[0],frames)
        logger.info('\033[92m SPE file written!\033[0m')
        

    def _errorDialog(self,text,info=""):
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Critical)
        msg.setText(text)
        msg.setInformativeText(info)
        msg.setWindowTitle("Error")
        msg.setStandardButtons(QtGui.QMessageBox.Ok)
        retval = msg.exec_()

    def _dockData(self):
        # Create Dock with ImageView widget
        width_proj_plots=100
        self.dockImg = Dock("Data")

        w1 = pg.GraphicsLayoutWidget()

        # A plot area (ViewBox + axes) for displaying the image
        self.py = w1.addPlot()
        self.py.setMaximumWidth(width_proj_plots)
        self.p1 = w1.addPlot()

        # Item for displaying image data
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)
        self.p1.addItem(self.vLine, ignoreBounds=True)
        self.p1.addItem(self.hLine, ignoreBounds=True)

        # Contrast/color control
        self.hist = pg.HistogramLUTItem()
        self.hist.setImageItem(self.img)
        w1.addItem(self.hist)

        # Another plot area for displaying ROI data
        w1.nextRow()
        w1.nextCol()
        self.px = w1.addPlot(colspan=1)
        self.px.setMaximumHeight(width_proj_plots)
        self.img.hoverEvent = self.imageHoverEvent
        w1.nextRow()

        self.jtime = 0
        self.x = 0
        self.y = 0
        self.timeLine = QSlider(Qt.Horizontal, self)
        self.timeLine.setRange(0, 1)
        self.timeLine.setValue(self.jtime)
        self.timeLine.setFocusPolicy(Qt.NoFocus)
        self.timeLine.setPageStep(1)
        self.timeLine.valueChanged.connect(self._timeChanged)

        self.dockImg.addWidget(w1)
        self.dockImg.addWidget(self.timeLine)

    def _timeChanged(self):
        self.jtime = self.timeLine.value()-1
        self._update_img_plot()
        self._update_data()

    def keyPressEvent(self, event):
        if event.key()==Qt.Key_Right and self.timeLine.maximum() > 1:
            self.timeLine.setValue(self.timeLine.value() + 1)
        elif event.key()==Qt.Key_Left and self.timeLine.maximum() > 1:
            self.timeLine.setValue(self.timeLine.value() - 1)

    def _update_img_plot(self):
        self.hLine.setPos(self.y)
        self.vLine.setPos(self.x)
        if self.cameraThread.data.ndim > 2:
            val = self.cameraThread.data[self.jtime, self.x, self.y]
            self.py.plot(self.cameraThread.data[self.jtime,self.x,:],np.arange(self.cameraThread.data.shape[2]), clear=True)
            self.px.plot(np.arange(self.cameraThread.data.shape[1]),self.cameraThread.data[self.jtime,:,self.y], clear=True)
        else:
            val = self.cameraThread.data[self.x, self.y]
            self.py.plot(self.cameraThread.data[self.x,:],np.arange(self.cameraThread.data.shape[1]), clear=True)
            self.px.plot(np.arange(self.cameraThread.data.shape[0]),self.cameraThread.data[:,self.y], clear=True)
        self.p1.setTitle("pos: (%d, %d, %d) value: %g" % (self.jtime, self.x, self.y, val))

    def imageHoverEvent(self,event):
        if event.isExit(): 
            # set title as blank 
            self.p1.setTitle("") 
            return
        pos = event.pos()
        ppos = self.img.mapToParent(pos)
        self.x, self.y = np.int(ppos.x()), np.int(ppos.y())
        self._update_img_plot()

    def _dockROI(self):
        # Acquisition and Focus
        self.dockROI = Dock("ROI",size=(1,ydim))
        
        # add table 
        self.ROItable = QTableWidget(self)
        self.ROItable.setColumnCount(6)
        
        self.ROItable.rowCount()
        self.ROItable.setHorizontalHeaderLabels(['x_start','width','x_binning','y_start','height','y_binning'])
        header = self.ROItable.horizontalHeader()
        header.setResizeMode(QtGui.QHeaderView.ResizeToContents)
        
    
        addBtn = QtGui.QPushButton('Add ROI')
        addBtn.clicked.connect(self._add_ROI)
        rmBtn = QtGui.QPushButton('Remove ROI')
        rmBtn.clicked.connect(self._remove_ROI)
        self.dockROI.addWidget(self.ROItable)
        self.dockROI.addWidget(addBtn)
        self.dockROI.addWidget(rmBtn)

    def _remove_ROI(self):
        row = self.ROItable.rowCount()
        self.ROItable.removeRow(row-1)

    def _add_ROI(self):
        row = self.ROItable.rowCount()
        self.ROItable.setRowCount(row+1)
        col = 0
        if row == 0:
            row_data = [0,512,1,0,512,1]
        else:
            new_y = int(self.ROItable.item(row-1, 3).text())+int(self.ROItable.item(row-1, 4).text())
            new_height = int(self.ROItable.item(row-1, 4).text())
            new_binning = int(self.ROItable.item(row-1, 5).text())
            row_data = [0,512,1,new_y,new_height,new_binning]
        for item in row_data:
            cell = QTableWidgetItem(str(item))
            self.ROItable.setItem(row, col, cell)
            col += 1

    def _dockMotor(self):
        # Acquisition and Focus
        self.dockMotor = Dock("Motor",size=(1,ydim))

    def _dockParams(self):
        self.tableWidget = QTableWidget()
        self.tableWidget.setRowCount(len(self.parameters.keys())-1)
        self.tableWidget.setColumnCount(2)
        jrow = 0
        for k,v in self.parameters.items():
            if k == 'ROIs':
                continue
            self.tableWidget.setItem(jrow,0, QTableWidgetItem(k))
            if hasattr(v,"value_str"):
                self.tableWidget.setItem(jrow,1, QTableWidgetItem("%s"%v.value_str))
            else:
                self.tableWidget.setItem(jrow,1, QTableWidgetItem("%s"%v.value))
            jrow += 1
        self.tableWidget.move(0,0)
        ## Acquisition and Focus
        self.dockParams = Dock("Full Parameters",size=(1,ydim))
        self.dockParams.addWidget(self.tableWidget)
        updateBtn = QtGui.QPushButton('Update')
        updateBtn.clicked.connect(self._update_params)
        self.dockParams.addWidget(updateBtn)

    def _adc_control(self, text):
        self.parameters["ADC Quality"].set_from_widget()
        self.parameters["ADC Speed"].set_from_widget()
        if not self.cameraThread.cam.validate_parameter(self.parameters["ADC Speed"].i):
            print("Speed Not Valid")
        if not self.cameraThread.cam.validate_parameter(self.parameters["ADC Quality"].i):
            print("Quality Not Valid")

    def _dockExpSetup(self):
        setup_parameters = ["Sensor Temperature Set Point", "Disable Cooling Fan","break",
                "Readout Control Mode","ADC Quality","ADC Analog Gain","ADC Speed","EM Gain","Normalize Orientation","Vertical Shift Rate","break",\
                "Shutter Timing Mode", "Trigger Response", "Trigger Determination", "Clean Before Exposure", "Clean Serial Register","Clean Until Trigger","Output Signal","Correct Pixel Bias","break",\
                "Exposure Time", "Readout Count"
                ]
        vlayout = QtGui.QVBoxLayout()
        for p in setup_parameters:
            if p == 'break':
                vlayout.addWidget(QHSeperationLine())
            else:
                vlayout.addLayout(self.parameters[p].layout)
        #lbl = QtGui.QLabel()
        #lbl.setText("N. frames")
        #self.nframesLine = QtGui.QLineEdit()
        #self.nframesLine.setText(str(10))
        #self.nframesLine.setFixedWidth(70)
        #self.nframesLine.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout = QtGui.QHBoxLayout()
        #layout.addWidget(lbl)
        #layout.addWidget(self.nframesLine)
        #vlayout.addLayout(layout)
        # If ADC Quality changes, change ADC speed accordingly
        self.parameters['ADC Quality'].combo.currentTextChanged.connect(self._adc_control)
        self.parameters['ADC Speed'].combo.currentTextChanged.connect(self._adc_control)

        #ROIs
        self.ROIbox = QtGui.QCheckBox("Use ROI",self)
        vlayout.addWidget(self.ROIbox)
        vlayout.addWidget(QHSeperationLine())

        # Acquisition and Focus
        self.dockExpSetup = Dock("Setup",size=(1,ydim))
        loadBtn = QtGui.QPushButton('Load')
        loadBtn.clicked.connect(self._load)
        saveBtn = QtGui.QPushButton('Save')
        saveBtn.clicked.connect(self._save)
        acquireBtn = QtGui.QPushButton('Acquire')
        acquireBtn.clicked.connect(self._acquire)
        focusBtn = QtGui.QPushButton('Focus')
        focusBtn.clicked.connect(self._focus)
        configureBtn = QtGui.QPushButton('Configure')
        configureBtn.clicked.connect(self._configure)
        
        saveSPEBtn = QtGui.QPushButton('Save SPE file')
        saveSPEBtn.clicked.connect(self._saveSPE)
        
        stopBtn = QtGui.QPushButton('Stop')
        stopBtn.clicked.connect(self._stop)

        w1 = QtGui.QWidget()
        vlayout.addStretch(1)
        vlayout.addWidget(loadBtn)
        vlayout.addWidget(saveBtn)
        vlayout.addWidget(configureBtn)
        vlayout.addWidget(saveSPEBtn)
        vlayout.addWidget(QHSeperationLine())
        vlayout.addWidget(acquireBtn)
        vlayout.addWidget(focusBtn)
        vlayout.addWidget(stopBtn)
        w1.setLayout(vlayout)

        self.dockExpSetup.addWidget(w1)
